IM-Matcher-Hinter
=================
A hinter plugin for Vimperator. It let you use CJK input method inital characters as hint character.

Chinese input methods like PinYin, Wubi, Double PinYin can be used as hint characters!

Installation
============
unpack the content of the plugin into ~/.vimperator/plugin/

More Input Method data JSON files can be generated with input method table file
and the `im-matcher-table-gen.py` program found in [im-matcher project].


Preference
==========
Plugin preferences can be set in ~/.vimperator as global variables:
```
let option001 = "something"
```

Available preferences:

 * hintimmatcherpath = "~/.vimperator/plugin/im-matcher-hinter/im-matcher-table.json"


DEBUG
=====
In vimperator, at the command line, type `:message` to see log messages

[im-matcher project]: https://gitlab.com/mozbugbox/im-matcher/tree/master

