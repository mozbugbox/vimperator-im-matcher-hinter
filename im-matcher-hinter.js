/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * The hintimmatcherpath is a string variable which can set on
 * vimperatorrc as following.
 *
 * It can be set in .vimperatorrc file as:
 *
 * let hintimmatcherpath="~/.mydata/im-matcher-table-wubi.json"
 *
 * The json file can be generated using the `im-matcher-table-gen.py`
 * program find in `im-matcher` project.
 *
 * The im table file will be searched in dirs: `~/.vimperator`,
 * "the hint matcher plugin directory", and `~/.local/share/im-matcher`.
 *
 * im-matcher: https://gitlab.com/mozbugbox/im-matcher
 */

// Idea From: https://code.google.com/archive/p/pinyin-hints-vimperator/
// vimperator API: http://seesaawiki.jp/w/shin_yan/d/liberator
// https://github.com/vimperator/vimperator-labs/tree/master/common/content

var version="0.3"
/**
 * Override _hintMatcher() which, when hintmatching=contains, returns a
 * function that does pinyin string matching.
 *
 */

if (liberator) {
    vimpobj = liberator;
} else if (dactyl) {
    vimpobj = dactyl;
}

vimpobj.modules.hints._hintMatcher_orig = vimpobj.modules.hints._hintMatcher;

var plugins = vimpobj.plugins;
var im_matcher = null;
var NAME = "IM-Matcher-Hinter";

function check_dir_file(filename, dir_list) {
    // check if a filename exists in a list of directories
    var full_path, fobj;
    var ret = null;
    if (!filename) {
        return ret;
    }
    var isdir = function(x) {return x.exists() && x.isDirectory()};
    var isfile = function(x) {return x.exists() && x.isFile()};
    fobj = io.File(filename);
    if (io.File.isAbsolutePath(filename) && isfile(fobj)) {
        ret = fobj;
        return ret;
    }
    dirs = [];
    for (var dirname of dir_list) {
        dirname = io.File.expandPath(dirname);
        fobj = io.File(dirname);
        if (isdir(fobj)) {
            dirs.push(dirname);
        }
    }
    for (var dirname of dirs) {
        full_path = OS.Path.join(dirname, filename);
        fobj = io.File(full_path);
        if (isfile(fobj)) {
            ret = fobj;
            break;
        }
    }
    return ret;
}

function load_immatcher() {
    // Load im matcher data file
    //vimpobj.echomsg("Loading matcher...");
    var imm = -1;
    var im_filename = "im-matcher-table.json";
    var std_im_dir = "~/.local/share/im-matcher";
    var hintm_dir = OS.Path.dirname(plugins.imMatcherHinter.PATH);
    var rcpath = vimpobj.globalVariables.hintimmatcherpath;

    var imfile = null;
    var path = null;
    var dir_list = ["~/.vimperator", hintm_dir, std_im_dir];
    imfile = check_dir_file(rcpath, dir_list);
    if (imfile === null) {
        imfile = check_dir_file(im_filename, dir_list);
    }
    if (imfile !== null) {
        var im_json_data = imfile.read();
        var imm = plugins.immatcher.create_matcher(im_json_data);
        vimpobj.echomsg("IM-Matcher File: " + imfile.path);
    } else {
        vimpobj.echoerr("Failed to find IM-Matcher File to load.");
    }

    return imm;
}

/**
 * Divide a string by a regular expression.
 *
 * @param {RegExp|string} pat The pattern to split on.
 * @param {string} str The string to split.
 * @return {Array(string)} The lowercased splits of the splitting.
 */
function tokenize(pat, str) {
    return str.split(pat).map(String.toLowerCase);
}

/**
 * Get a hint matcher for hintmatching=contains
 *
 * The hintMatcher expects the user input to be space delimited and it
 * returns true if each set of characters typed can be found, in any
 * order, in the link.
 *
 * @param {string} hintString  The string typed by the user.
 * @return {function(String):boolean} A function that takes the text
 *     of a hint and returns true if all the (space-delimited) sets of
 *     characters typed by the user can be found in it.
 */
function IMContainsMatcher(hintString) {
    var tokens = tokenize(/\s+/, hintString);
    return function(linkText) {
        linkText = linkText.toLowerCase();
        return tokens.every(function(token) {
            return im_matcher.find(linkText, token) >= 0;
        });
    };
}

/**
 * Override _hintMatcher
 */
vimpobj.modules.hints._hintMatcher = function(hintString) {
    var func;
    var hintopt = options.get("hintmatching").values[0];
    /* Initialize matching table */
    if (hintopt == "contains") {
        if (im_matcher === null) {
            im_matcher = load_immatcher();
        }
        if (im_matcher === -1) {
            func = this._hintMatcher_orig(hintString);
        } else {
            func = IMContainsMatcher(hintString);
        }
    } else {
        func = this._hintMatcher_orig(hintString);
    }
    return func;
};
// the function name of hintmatching=custom don't make sense to me.
//vimpobj.plugins.customHintMatcher = IMContainsMatcher;


// INFO {{{
var INFO = xml`
  <plugin name="IM-Matcher-Hinter" version="0.3"
          href="https://gitlab.com/mozbugbox/vimperator-im-matcher-hinter"
          summary="Use CJK input method (PinYin) initial char as hint char"
          lang="en-US"
          xmlns="http://vimperator.org/namespaces/liberator">
    <author email="mozbugbox@yahoo.com.au">mozbugbox</author>
    <license>GPL version 3 or later</license>
    <project name="Vimperator" minVersion="2.3"/>
    Input method for CJK can be used as hint characters.<p/>
    Chinese input methods: PinYin, Wubi, Double PinYin...<p/>
    Example: With PinYin data, The hint for "好好学习" will be "hhxx".
    <h3>List of Global Variables</h3>
    <item>
      <tags>hintimmatcherpath</tags>
      <spec>hintimmatcherpath</spec>
      <description>
        <type>string</type><default>im-matcher-table.json</default>
      <p/>
      A string variable from which load input method data for hint
      matcher with input method initial characters.<p/>
      
      It can be set in .vimperatorrc file as:
      <code>
      let hintimmatcherpath = "~/.mydata/im-matcher-table-wubi.json"
      </code>
 
      The JSON file can be generated using the im-matcher-table-gen.py
      program found in <link topic="https://gitlab.com/mozbugbox/im-matcher">im-matcher</link> project.<p/>
 
      The im table file will be searched in dirs: ~/.vimperator,
      "the hint matcher plugin directory", and ~/.local/share/im-matcher.<p/>
      </description>
    </item>
  </plugin>`;
// }}}

// vim: set fileencoding=utf-8 fdm=marker sw=4 ts=4 et:
