function ՐՏ_Iterable(iterable) {
    if (Array.isArray(iterable) || iterable instanceof String || typeof iterable === "string") {
        return iterable;
    }
    return Object.keys(iterable);
}
function ՐՏ_bind(fn, thisArg) {
    var ret;
    if (fn.orig) {
        fn = fn.orig;
    }
    if (thisArg === false) {
        return fn;
    }
    ret = function() {
        return fn.apply(thisArg, arguments);
    };
    ret.orig = fn;
    return ret;
}
function range(start, stop, step) {
    var length, idx, range;
    if (arguments.length <= 1) {
        stop = start || 0;
        start = 0;
    }
    step = arguments[2] || 1;
    length = Math.max(Math.ceil((stop - start) / step), 0);
    idx = 0;
    range = new Array(length);
    while (idx < length) {
        range[idx++] = start;
        start += step;
    }
    return range;
}
function len(obj) {
    if (Array.isArray(obj) || typeof obj === "string") {
        return obj.length;
    }
    return Object.keys(obj).length;
}
function eq(a, b) {
    var i;
    "\n    Equality comparison that works with all data types, returns true if structure and\n    contents of first object equal to those of second object\n\n    Arguments:\n        a: first object\n        b: second object\n    ";
    if (a === b) {
        return true;
    }
    if (Array.isArray(a) && Array.isArray(b) || a instanceof Object && b instanceof Object) {
        if (a.constructor !== b.constructor || a.length !== b.length) {
            return false;
        }
        if (Array.isArray(a)) {
            for (i = 0; i < len(a); i++) {
                if (!eq(a[i], b[i])) {
                    return false;
                }
            }
        } else {
            var ՐՏ_Iter0 = ՐՏ_Iterable(a);
            for (var ՐՏ_Index0 = 0; ՐՏ_Index0 < ՐՏ_Iter0.length; ՐՏ_Index0++) {
                i = ՐՏ_Iter0[ՐՏ_Index0];
                if (!eq(a[i], b[i])) {
                    return false;
                }
            }
        }
        return true;
    }
    return false;
}
function ՐՏ_in(val, arr) {
    if (Array.isArray(arr) || typeof arr === "string") {
        return arr.indexOf(val) !== -1;
    } else {
        if (arr.hasOwnProperty(val)) {
            return true;
        }
        return false;
    }
}
function dir(item) {
    var arr;
    arr = [];
    for (var i in item) {
        arr.push(i);
    }
    return arr;
}
function ՐՏ_extends(child, parent) {
    child.prototype = Object.create(parent.prototype);
    child.prototype.constructor = child;
}

var __name__ = "__main__";

"\nSearch Chinese text using InputMethod initials\n\nCopyright (C) 2016 copyright <mozbugbox@yahoo.com.au>\n\nThis program is free software; you can redistribute it and/or modify\nit under the terms of the GNU General Public License as published by\nthe Free Software Foundation; either version 2 of the License, or\n(at your option) any later version.\n\nThis program is distributed in the hope that it will be useful,\nbut WITHOUT ANY WARRANTY; without even the implied warranty of\nMERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the\nGNU General Public License for more details.\n\nYou should have received a copy of the GNU General Public License\nalong with this program; if not, see <http://www.gnu.org/licenses/>.\n\nUsage:\n    im_matcher = create_matcher(im_data_json)\n    txt = \"红花不是,随便的花草鱼虫\"\n    index = im_matcher.find(txt, \"hh\")\n    if index >= 0:\n        print(\"found\")\n    else:\n        print(\"not found\")\n";
__version__ = "0.2";
CACHE_SIZE = 512;
TABLE_SEPERATOR = "|";
UNICODE_REGION = {
    "hani_comp": [ 63744, 64255 ],
    "hani_comp_sup": [ 194560, 195103 ],
    "hani_bmp": [ 19968, 40959 ],
    "hani_exta": [ 13312, 19903 ],
    "hani_extb": [ 131072, 173791 ],
    "hani_extc": [ 173824, 177983 ],
    "hani_extd": [ 177984, 178207 ],
    "hani_exte": [ 178208, 183983 ]
};
NUMS_ZH = "零一二三四五六七八九";
function decode_im_table(im_table_data) {
    var result, im_list;
    "decode a string of im_table to list of im codes";
    function _expand_rle(seg) {
        return seg[seg.length - 1].repeat(seg.slice(0, seg.length - 1));
    }
    result = im_table_data.replace(new RegExp("\\d+[A-Za-z" + TABLE_SEPERATOR + "]", "g"), _expand_rle);
    function _expand_sep(m) {
        return m.toLowerCase() + TABLE_SEPERATOR;
    }
    result = result.replace(/[A-Z]/g, _expand_sep, result);
    im_list = result.split(TABLE_SEPERATOR);
    return im_list;
}
function IMMatcher() {
    IMMatcher.prototype.__init__.apply(this, arguments);
}
" Search Chinese text using InputMethod initials ";
IMMatcher.prototype.__init__ = function __init__(im_table_dict){
    var self = this;
    if (typeof im_table_dict === "undefined") im_table_dict = null;
    var v, k;
    self.im_table = null;
    if (im_table_dict !== null) {
        self.im_table = {};
        var ՐՏ_Iter1 = ՐՏ_Iterable(im_table_dict);
        for (var ՐՏ_Index1 = 0; ՐՏ_Index1 < ՐՏ_Iter1.length; ՐՏ_Index1++) {
            k = ՐՏ_Iter1[ՐՏ_Index1];
            v = im_table_dict[k];
            self.im_table[k] = decode_im_table(v);
        }
    }
};
IMMatcher.prototype.match_charcode_region = function match_charcode_region(tcode, key_char, reg){
    var self = this;
    var ՐՏ_Unpack, start, end, ret, ՐՏ_Temp;
    "Match charcode to a unicode region";
    ret = false;
    ՐՏ_Unpack = UNICODE_REGION[reg];
    start = ՐՏ_Unpack[0];
    end = ՐՏ_Unpack[1];
    try {
        if (start <= tcode && tcode <= end) {
            ret = ՐՏ_in(key_char, self.im_table[reg][tcode - start]);
        }
    } catch (ՐՏ_Exception) {
        if (ՐՏ_Exception instanceof IndexError) {
            ret = false;
        } else {
            throw ՐՏ_Exception;
        }
    }
    return ret;
};
IMMatcher.prototype.match_char = function match_char(target, key_char){
    var self = this;
    var tcode, im_table, match_charcode_region, res, region;
    "Match a input keycode to IM codes of target";
    if (target === key_char) {
        return true;
    }
    if (self.im_table === null) {
        return false;
    }
    if ("0" <= target && target <= "9") {
        target = NUMS_ZH[parseInt(target)];
    }
    tcode = target.charCodeAt(0);
    im_table = self.im_table;
    match_charcode_region = ՐՏ_bind(self.match_charcode_region, self);
    var ՐՏ_Iter2 = ՐՏ_Iterable(im_table);
    for (var ՐՏ_Index2 = 0; ՐՏ_Index2 < ՐՏ_Iter2.length; ՐՏ_Index2++) {
        region = ՐՏ_Iter2[ՐՏ_Index2];
        res = match_charcode_region(tcode, key_char, region);
        if (res) {
            break;
        }
    }
    return res;
};
IMMatcher.prototype.find = function find(txt, sub, start, end){
    var self = this;
    if (typeof start === "undefined") start = 0;
    if (typeof end === "undefined") end = null;
    var sub_len, match_char, matched, i, n;
    "Customized version of string.find to match IM code";
    if (end === null) {
        end = len(txt);
    } else if (end < 0) {
        end = len(txt) + end;
    }
    matched = txt.indexOf(sub, start, end);
    if (matched >= 0) {
        return matched;
    }
    if (self.im_table === null) {
        return -1;
    }
    sub_len = len(sub);
    match_char = ՐՏ_bind(self.match_char, self);
    for (n = start; n < end - sub_len + 1; n++) {
        matched = n;
        for (i = 0; i < sub_len; i++) {
            if (!match_char(txt[n + i], sub[i])) {
                matched = -1;
                break;
            }
        }
        if (matched >= 0) {
            break;
        }
    }
    return matched;
};
IMMatcher.prototype.contain = function contain(txt, sub){
    var self = this;
    "test if a string contains substring, include IM code";
    return self.find(txt, sub) >= 0;
};

function create_matcher(im_data_json) {
    var im_dict, matcher;
    "Create a IMMatcher from im data in json";
    im_dict = JSON.parse(im_data_json);
    matcher = new IMMatcher(im_dict["im_table"]);
    return matcher;
}
//exports.create_matcher = create_matcher;
//exports.IMMatcher = IMMatcher;
