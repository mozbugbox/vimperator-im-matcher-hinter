#!/bin/sh
Name=im-matcher-hinter
Version=`grep -e "^var version\s*=" ${Name}.js |sed 's/^.*"\(.*\)".*$/\1/'`

Build_Dir=${Name}-${Version}
echo ${Build_Dir}
if [ -e ${Build_Dir} ] ; then
    rm -rf ${Build_Dir}
fi

Files="
    im-matcher-hinter.js 
    immatcher.js
    im-matcher-table*.json 
    README.md
    LICENSE
"

mkdir ${Build_Dir}
cp ${Files} ${Build_Dir}

OUTPUT_NAME="${Build_Dir}.tar.xz"
/bin/rm "${OUTPUT_NAME}"
/bin/tar --numeric-owner --auto-compress -cvf "${OUTPUT_NAME}"  "${Build_Dir}"
/bin/rm -rf ${Build_Dir}
echo "Saved to ${OUTPUT_NAME}"
